/**
 * @fileOverview  The MySQL storage adapter method library
 * @author Mircea Diaconescu
 * @copyright Copyright 2015 Mircea Diaconescu, Chair of Internet Technology,
 *   Brandenburg University of Technology, Germany.
 * @license The MIT License (MIT)
 */
module.exports = {
  pool: null,
  /**
   * Get the connection with the database (async method).
   * @method
   * @param {function} callback The function that is invoked after
   * the async operation has completed.
   */
  getConnection: function ( callback) {
    var sqlStatement = '', storageAdapter = this.currentAdapter;
    if ( !this.pool) {
      this.pool = mysqlConnector.createPool({
        host     : this.currentAdapter.host,
        user     : this.currentAdapter.user,
        password : this.currentAdapter.password
      });
    }
    this.pool.getConnection( function ( err, connection) {
      if ( err) {
        callback( err, null);
      } else {
        sqlStatement = 'USE ' + storageAdapter.database + ';';
        connection.query( sqlStatement, function ( err) {
          if ( err) {
            callback( err, null);
          } else {
            callback( null, connection);
          }
        });
      }
    });
  },

  /**
   * Generic method for loading/retrieving a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {string|number} id  The object ID value
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  retrieve: function ( mc, id, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name),
      stdid = mc.standardId,
      url = '';
    if ( id === null || id === undefined) {
      throw "sTORAGEmANAGER['MySQL'].retrieve: undefined or null object ID!";
    }
    // client side: make GET request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {
        url += ':' + window.location.port;
      }
      xhr.GET({
        url: url + '/' + tableName + '/' + id,
        handleResponse: function ( r) {
          var response = null, obj = null;
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              response = JSON.parse( r.responseText);
              obj = mc.convertRec2Obj( response);
              continueProcessing( obj, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MySQL'].retrieve: fatal exception "
              + "occurred during the 'retrieve' MySQL request!";
          }
        }
      });
    }
    // server side: make MySQL query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function ( err, dbConnection) {
          var sqlStatement = '';
          if ( err) {
            continueProcessing( null, err);
          } else {
            sqlStatement = "SELECT * FROM " + tableName + " WHERE `" + util.getColumnName( stdid) + "` = ?;";
            dbConnection.query( sqlStatement, [id], function ( err, rows) {
              var row = {};
              if ( err) {
                continueProcessing( null, err);
              } else {
                try {
                  if ( rows.length > 0) {
                    // convert property names from underscore notation to CamelCase notation
                    Object.keys( rows[0]).forEach( function( pName) {
                      row[util.getPropertyName( pName)] = rows[0][pName];
                    });
                    continueProcessing( row, null);
                  } else {
                    continueProcessing( null, null);
                  }
                } catch ( e) {
                  continueProcessing( null, e);
                }
              }
            });
          }
        });
      }
    }
  },
 /**
  * Generic method for loading/retrieving all instances for a model object
  * @method
  * @param {object} mc  The model class concerned
  * @param {function} continueProcessing  The function that is invoked after
  * the async operation has completed. Its first parameter is the resulting data,
  * if any, and its second optional parameter is an error message/object.
  */
  //------------------------------------------------
  retrieveAll: function ( mc, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name),
      stdid = mc.standardId,
      url = '';
    // client side: make GET request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {
        url += ':' + window.location.port;
      }
      xhr.GET({
        url: url + '/' + tableName,
        handleResponse: function ( r) {
          var response = null, objs = [];
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              response = JSON.parse( r.responseText);
              for ( i = 0; i < response.length; i++) {
                objs[i] = mc.convertRec2Obj( response[i]);
                if ( objs[i]) mc.instances[response[i][stdid]] = objs[i];
              }
              continueProcessing( objs, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MySQL'].retrieveAll: fatal exception "
              + "occurred during the 'retrieveAll' MySQL request!";
          }
        }
      });
    }
    // server side: make MySQL query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function ( err, dbConnection) {
          var sqlStatement = '';
          if ( err) {
            continueProcessing( null, err);
          } else {
            sqlStatement = "SELECT * FROM " + tableName + ";";
            dbConnection.query( sqlStatement, function ( err, rows) {
              var objs = [];
              if ( err) {
                continueProcessing( null, err);
              } else {
                for ( i = 0; i < rows.length; i++) {
                  // convert property names from underscore notation to CamelCase notation
                  objs[i] = {};
                  Object.keys( rows[i]).forEach( function ( pName) {
                    objs[i][util.getPropertyName( pName)] = rows[i][pName];
                  });
                }
                continueProcessing( objs, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for creating a new instance for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {object} slots  The slots for object creation
   * @param {object} newObj  The new object
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  add: function ( mc, slots, newObj, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name), url = '';
    // client side: make POST request
    if ( typeof exports !== 'object') {
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {
        url += ':' + window.location.port;
      }
      xhr.POST({
        url: url + '/' + tableName,
        msgBody: JSON.stringify( slots),
        reqFormat: 'application/json',
        handleResponse: function ( r) {
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              if ( typeof continueProcessing === 'function') continueProcessing( newObj, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              if ( typeof continueProcessing === 'function') {
                continueProcessing( newObj, "Error " + r.status + ": " + r.statusText);
              }
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MySQL'].add: fatal exception "
                + "occurred during the 'add' MySQL request!";
          }
        }
      });
    }
    // server side: make MySQL query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function (err, dbConnection) {
          var sqlStatement = '', params = [], qM = '';
          if ( err) {
            continueProcessing( null, err);
          } else {
            // create the INSERT statement
            sqlStatement = "INSERT INTO " + tableName + " ( ";
            Object.keys( newObj.type.properties).forEach( function ( propName, index) {
              if ( index > 0) {
                sqlStatement += ', ';
                qM += ', ';
              }
              sqlStatement += "`" + util.getColumnName( propName) + "`";
              qM += '?';
              // property value
              params.push( newObj[propName]);
            });
            sqlStatement += ') VALUE ( ';
            sqlStatement += qM + ');';
            dbConnection.query( sqlStatement, params, function ( err) {
              if ( err) {
                continueProcessing( null, err);
              } else {
                continueProcessing( null, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for update a instance for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {string|number} id  The object ID value
   * @param {object} slots  The slots for object update
   * @param {object} newObj  The updated object
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  /***** newObj includes validated update slots *****/
  update: function ( mc, id, slots, newObj, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name), url = '';
    // client side: make PUT request
    if ( typeof exports !== 'object') {
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {
        url += ':' + window.location.port;
      }
      xhr.PUT({
        url: url + '/' + tableName + '/' + id,
        msgBody: JSON.stringify( slots),
        reqFormat: 'application/json',
        handleResponse: function ( r) {
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              if ( typeof continueProcessing === 'function') continueProcessing( newObj, null);
            } else {
              console.log( "Error " + r.status +": "+ r.statusText);
              if ( typeof continueProcessing === 'function') {
                continueProcessing( newObj, "Error " + r.status +": "+ r.statusText);
              }
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MySQL'].update: fatal exception "
                + "occurred during the 'update' MySQL request!";
          }
        }
      });
    }
    // server side: make MySQL query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function (err, dbConnection) {
          var sqlStatement = '', params = [], qM = '', i = 0;
          if ( err) {
            continueProcessing( null, err);
          } else {
            // create the UPDATE statement
            sqlStatement = "UPDATE " + tableName + " SET ";
            Object.keys( slots).forEach( function ( propName, index) {
              if ( typeof slots[propName] !== 'function') {
                if ( i > 0) {
                  sqlStatement += ', ';
                }
                sqlStatement += "`" + util.getColumnName( propName) + "`" + ' = ?';
                // property value
                params.push( slots[propName]);
                i++;
              }
            });
            sqlStatement += " WHERE `" + util.getColumnName( mc.standardId) + "`= ?" ;
            params.push( id);
            dbConnection.query( sqlStatement, params, function ( err) {
              if ( err) {
                continueProcessing( null, err);
              } else {
                continueProcessing( null, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for deletion of an instance for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {string|number} id  The object ID value
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  destroy: function ( mc, id, continueProcessing) {
  //------------------------------------------------
    var tableName = util.getTableName( mc.name), url = '';
    if ( id === null || id === undefined) {
      throw "sTORAGEmANAGER['MySQL'].destroy: undefined or null object ID!";
    }
    // client side: make DELETE request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {
        url += ':' + window.location.port;
      }
      xhr.DELETE({
        url: url + '/' + tableName + '/' + id,
        handleResponse: function ( r) {
          var response = null;
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              continueProcessing( null, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MySQL'].destroy: fatal exception "
                + "occurred during the 'destroy' MySQL request!";
          }
        }
      });
    }
    // server side: make MySQL query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      if ( typeof continueProcessing === 'function') {
        this.getConnection( function ( err, dbConnection) {
          var sqlStatement = '';
          if ( err) {
            continueProcessing( null, err);
          } else {
            sqlStatement = "DELETE FROM " + tableName + " WHERE `" + util.getColumnName( mc.standardId) + "` = ?;";
            dbConnection.query( sqlStatement, [id], function ( err, row) {
              if ( err) {
                continueProcessing( null, err);
              } else {
                continueProcessing( row, null);
              }
            });
          }
        });
      }
    }
  },
  /**
   * Generic method for deletion of al instances for a model object
   * @method
   * @param {object} mc  The model class concerned
   * @param {function} continueProcessing  The function that is invoked after
   * the async operation has completed. Its first parameter is the resulting data,
   * if any, and its second optional parameter is an error message/object.
   */
  //------------------------------------------------
  /* Delete all the entries for the specified type.
   */
  clearData: function ( mc, continueProcessing) {
  //------------------------------------------------
  var tableName = util.getTableName( mc.name), url = '';
    // client side: make DELETE request
    if ( typeof exports !== 'object') {
      if ( typeof continueProcessing !== 'function') {
        continueProcessing = function ( obj, error) {};
      }
      url = window.location.protocol + '//' + window.location.hostname;
      if ( window.location.port > 0 && window.location.port !== 80) {
        url += ':' + window.location.port;
      }
      xhr.DELETE({
        url: url + '/' + tableName + '/',
        handleResponse: function ( r) {
          var response = null;
          try {
            if ( r.status === 200 || r.status === 201 || r.status === 304) {
              continueProcessing( null, null);
            } else {
              console.log( "Error " + r.status + ": " + r.statusText);
              continueProcessing( null, "Error " + r.status + ": " + r.statusText);
            }
          } catch ( e) {
            console.log( e);
            throw "sTORAGEmANAGER['MySQL'].clearData: fatal exception "
                + "occurred during the 'clearData' MySQL request!";
          }
        }
      });
    }
    // server side: make MySQL query
    else {
      // makes no sense to execute SQL statements if is nothing to do with the response...
      this.getConnection( function ( err, dbConnection) {
        var sqlStatement = '';
        if ( err) {
          continueProcessing( null, err);
        } else {
          sqlStatement = "DELETE FROM " + tableName + ";";
          dbConnection.query( sqlStatement, function ( err, row) {
            if ( err) {
              if ( typeof continueProcessing === 'function') continueProcessing( null, err);
            } else {
              if ( typeof continueProcessing === 'function') continueProcessing( null, null);
            }
          });
        }
      });
    }
  }
};