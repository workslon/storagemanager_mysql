[TOC]

# Overview

NPM module - adapted version of [`sTORAGEmANAGER_MySQL.js`](https://bitbucket.org/gwagner57/ontojs/src/badaa61994396729f101696f55b5249ef78e4c5c/src/sTORAGEmANAGER_MySQL.js) written by the chair of Prof. Gerd Wagner, BTU Cottbus

You can find the source code [here](https://bitbucket.org/workslon/storagemanager_mysql/src/6070442f91764449f982c6128968b935d0532527/index.js)

# Dependencies

Requires and extends [sTORAGEmANAGER.js](https://bitbucket.org/workslon/storagemanager)

# Usage

MySQL storage manager adapter is a part of [sTORAGEmANAGER.js](https://bitbucket.org/workslon/storagemanager). It extends its default functionality and provides with MySQL instead of default LocalStorage API.

So, to use it you need to install [sTORAGEmANAGER.js](https://bitbucket.org/workslon/storagemanager) and configure it to use MySQL adapter. See examples below.

## CommonJS

```javascript
var sTORAGEmANAGER = require('storage-manager');
var mysqlStorageManager = new sTORAGEmANAGER({
  name: 'MySQL',
  pool: false
});

```

## ES6 Modules

```javascript
import sTORAGEmANAGER from 'storage-manager';
const mysqlStorageManager = new sTORAGEmANAGER({
  name: 'MySQL',
  pool: false
})
// ...
```

# API References

## Methods

__.getConnection(callback)__ -  Get the connection with the database (async method)

| Name     | Type     | Description       |
|----------|----------|-------------------|
| callback | Function | Callback function |

__.retrieve(modelClass, id, continueProcessing)__ - Generic method for loading/retrieving a model object

| Name               | Type     | Description                         |
|--------------------|----------|-------------------------------------|
| modelClass         | Object   | mODELcLASS instance                 |
| id                 | String   | Id of the target object in database |
| continueProcessing | Function | Callback function                   |

__.retrieveAll(modelClass, continueProcessing)__ - Generic method for loading/retrieving all models

| Name               | Type     | Description                         |
|--------------------|----------|-------------------------------------|
| modelClass         | Object   | mODELcLASS instance                 |
| continueProcessing | Function | Callback function                   |

__.add(modelClass, slots, newObj, continueProcessing)__

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| slots              | Object   | The slots for object creation                 |
| newObj             | Object   | The new object                                |
| continueProcessing | Function | Callback function                             |

__.update(modelClass, objectId, slots, newObj, continueProcessing)__

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| objectId           | String   | ID of the object to be updated                |
| slots              | Object   | The slots for object creation                 |
| newObj             | Object   | The new object                                |
| continueProcessing | Function | Callback function                             |

__.destroy(modelClass, objectId, continueProcessing)__

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| objectId           | String   | ID of the object to be destroyed              |
| continueProcessing | Function | Callback function                             |

__.clearData(modelClass, continueProcessing)__ - delete all entries

| Name               | Type     | Description                                   |
|--------------------|----------|-----------------------------------------------|
| modelClass         | Object   | mODELcLASS instance                           |
| continueProcessing | Function | Callback function                             |